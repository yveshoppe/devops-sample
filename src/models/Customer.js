const mongoose = require("mongoose");

const Customer = new mongoose.Schema(
  {
    firstName: { type: String, required: true },
    lastName: { type: String, required: true, index: true },
    addresses: [
      {
        street: { type: String },
        city: { type: String },
        zip: { type: String },
      }
    ]
  },
  { timestamps: true }
);

const CustomerModel = mongoose.model("Customer", Customer);

module.exports = CustomerModel;
