const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const ansible = require("node-ansible");

const mongoose = require("mongoose");

const CustomerModel = require("./models/Customer");

const config = require("../config");

const app = express();

app.use(cors());

app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

app.use(express.json());

// Connect to MongoDB
mongoose.connect("mongodb://" + config.mongoHost + ":27017/cdb", {
  useNewUrlParser: true,
});

// respond with "hello world" when a GET request is made to the homepage
app.get("/", (req, res) => {
  res.send("<h1>Sample REST API</h1>");
});

let autoIncrement = 3;

app.get("/customers", async (req, res) => {
  const customers = await CustomerModel.find();

  res.json({ items: customers });
});

// Wenn jemand die URL /customers/1 aufruft, dann wird
// die Funktion hier aufgerufen (mit dem req, res)
app.get("/customers/:id", async (req, res) => {
  console.log(req.params.id);

  if (!req.params.id) {
    return res.status(400).json({ message: "No id" });
  }

  try {
    const objId = new mongoose.Types.ObjectId(req.params.id);

    const customer = await CustomerModel.findOne({ _id: objId });

    if (!customer) {
      return res.status(404).json({ message: "Customer not found" });
    }

    res.json({ item: customer });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Invalid _id" });
  }
});

app.post("/customers", async (req, res) => {
  console.log(req.body);

  if (!req.body) {
    return res.status(400).json({ message: "No body" });
  }

  if (!req.body.firstName) {
    return res.status(400).json({ message: "No first name" });
  }

  try {
    const newCustomer = new CustomerModel(req.body);

    await newCustomer.validate();
    await newCustomer.save();

    res.json({ message: "Customer created", item: newCustomer });
  } catch (error) {
    console.error(error);
    return res.status(400).json({ message: "Invalid data" });
  }
});

app.delete("/customers/:id", async (req, res) => {
  console.log(req.params.id);

  if (!req.params.id) {
    return res.status(400).json({ message: "No id" });
  }

  await CustomerModel.deleteOne({ _id: req.params.id });

  res.json({ message: "Customer deleted" });
});

app.put("/customers", async (req, res) => {
  console.log(req.body);

  if (!req.body) {
    return res.status(400).json({ message: "No body" });
  }

  if (!req.body.firstName) {
    return res.status(400).json({ message: "No first name" });
  }

  const customer = await CustomerModel.findOne({ _id: req.body._id });

  if (!customer) {
    return res.status(404).json({ message: "Customer not found" });
  }

  await customer.updateOne(req.body);

  res.json({ message: "Customer updated", item: customer });
});

app.post("/ansible", async (req, res) => {
  const command = req.body.command;

  if (!command) {
    return res.status(400).json({ message: "No command" });
  }

  const ansibleCommand = new ansible.AdHoc()
    .module("shell")
    .hosts("local")
    .user("root")
    .args(command);

  const result = await ansibleCommand.exec();

  res.json({ message: "Ansible executed", result });
});

app.listen(4000, () => {
  console.log(`Example app listening on port`);
});
