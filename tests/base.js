const assertArrays = require('chai-arrays');
const chai = require('chai');

chai.use(assertArrays);
const should = require('chai').should();
const supertest = require('supertest');

const config = {
  server: 'http://localhost:4000',
};

module.exports = {
  api: supertest(config.server),
  should: should,
  config: config,
};