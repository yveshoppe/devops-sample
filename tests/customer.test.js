const helper = require('./base');

// Supertest API Wrapper
const api = helper.api;

/**
 * Test Suite for Customers
 */
describe('Customers Tests', () => {
  it('Get should return the users settings', async () => {
    const result = await api
      .get('/customers')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .send();

    result.type.should.equal('application/json'); // Gibt die API JSON zurück?
    result.statusCode.should.equal(200); // Ist der STatuscode 200?

    result.body.should.have.property('items'); // Gibt es ein Property "items"?
  });

  it('POST with an invalid customer fails', async () => {
    const result = await api
      .post('/customers')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .send({asdf: 'asdf'});

    result.type.should.equal('application/json');
    result.statusCode.should.equal(400);
  });

  it('POST should work with a valid customer', async () => {
    const result = await api
      .post('/customers')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .send({firstName: 'Rafa', lastName: 'Nadal'});

    result.type.should.equal('application/json');
    result.statusCode.should.equal(200);

    // result.body.should.have('item');
    result.body.item.firstName.should.equal('Rafa');
  });
});